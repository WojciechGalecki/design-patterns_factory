package factory_zadanie2;

public class Bike {
    private EbikeModel name;
    private BIKE_TYPE type;
    private int gearNumber;

    public Bike(EbikeModel name, BIKE_TYPE type, int gearNumber) {
        this.name = name;
        this.type = type;
        this.gearNumber = gearNumber;
    }

    public EbikeModel getName() {
        return name;
    }

    public void setName(EbikeModel name) {
        this.name = name;
    }

    public BIKE_TYPE getType() {
        return type;
    }

    public void setType(BIKE_TYPE type) {
        this.type = type;
    }

    public int getGearNumber() {
        return gearNumber;
    }

    public void setGearNumber(int gearNumber) {
        this.gearNumber = gearNumber;
    }

    @Override
    public String toString() {
        return "Bike: " + name + " " + type + " " + gearNumber;
    }
}
