package factory_zadanie2;

public abstract class BikeFactory extends Bike {

    private BikeFactory(EbikeModel name, BIKE_TYPE type, int gearNumber) {
        super(name, type, gearNumber);
    }

    public static Bike createKross(){
        return new Bike(EbikeModel.KROSS,BIKE_TYPE.BICYCLE,5);
    }

    public static Bike createMerida(){
        return new Bike(EbikeModel.MERIDA,BIKE_TYPE.BICYCLE,6);
    }

    public static Bike createIniana(){
        return new Bike(EbikeModel.INIANA,BIKE_TYPE.TANDEM,3);
    }

    public static Bike createFelt(){
        return new Bike(EbikeModel.FELT,BIKE_TYPE.BICYCLE,6);
    }

    public static Bike createGoetze(){
        return new Bike(EbikeModel.GOETZE,BIKE_TYPE.TANDEM,1);
    }

}
