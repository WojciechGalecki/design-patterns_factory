package factory_zadanie2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Bike> list = new ArrayList<>();
        Bike b1 = BikeFactory.createKross();
        list.add(b1);
        Bike b2 = BikeFactory.createMerida();
        list.add(b2);
        Bike b3 = BikeFactory.createIniana();
        list.add(b3);
        Bike b4 = BikeFactory.createFelt();
        list.add(b4);
        Bike b5 = BikeFactory.createGoetze();
        list.add(b5);

        for (Bike bike:list) {
            System.out.println(bike);
        }

    }
}
