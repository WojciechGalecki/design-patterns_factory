package factory_zadanie3.main;

import factory_zadanie3.app.ApplicationFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Gimme name: ");
        String studentName = sc.nextLine();
        System.out.println("Gimme surname: ");
        String studentSurname = sc.nextLine();
        System.out.println("Gimme index number: ");
        String studentIndexNumberString = sc.nextLine();
        int studentIndexNumber = Integer.parseInt(studentIndexNumberString);

        Person applicant = new Person(studentName, studentSurname, studentIndexNumber);

        do {
            System.out.println("[apply] / [quit]");
            String command = sc.nextLine().toLowerCase().trim();
            if (command.equals("quit")) {
                System.out.println("EXIT");
                break;
            } else if (command.equals("apply")) {
                System.out.println("Application types: stay/schoolar/social/extend");
                command = sc.nextLine().toLowerCase().trim();
                if (command.equals("stay")) {
                    conditionalStayApp(sc, applicant);

                } else if (command.equals("schoolar")) {
                    schoolarshipApp(sc, applicant);

                } else if (command.equals("social")) {
                    socialApp(sc, applicant);

                } else if (command.equals("extend")) {
                    semesterExtendApp(sc, applicant);

                }
            } else {
                System.out.println("Wrong command!");
                continue;
            }
        } while (sc.hasNextLine());
    }

    private static void semesterExtendApp(Scanner sc, Person applicant) {
        System.out.println("Give me a reason: ");
        String reason = sc.nextLine();

        System.out.println(ApplicationFactory.createSemesterExtendApplication(applicant, reason));
        System.out.println("To exit --> quit");
    }

    private static void socialApp(Scanner sc, Person applicant) {
        System.out.println("Give me grades separated by comas, (ex. '2,3,4,5,5') : ");
        String[] studentGradesString = sc.nextLine().toLowerCase().trim().split(",");
        List<Double> grades = new ArrayList<>();

        for (String gradesString : studentGradesString) {
            grades.add(Double.parseDouble(gradesString));
        }

        System.out.println("Give me your total family incom (00.00) : ");
        String incomString = sc.nextLine().toLowerCase().trim().replace(",", ".");
        Double incom = Double.parseDouble(incomString);

        System.out.println(ApplicationFactory.createSocialSchoolarshipApplicatio(applicant, grades, incom));
        System.out.println("To exit --> quit");
    }

    private static void schoolarshipApp(Scanner sc, Person applicant) {
        System.out.println("Give me grades separated by comas, (ex. '2,3,4,5,5') : ");
        String[] studentGradesString = sc.nextLine().toLowerCase().trim().split(",");
        List<Double> grades = new ArrayList<>();

        for (String gradesString : studentGradesString) {
            grades.add(Double.parseDouble(gradesString));
        }

        System.out.println("Give me your extracurricular activetes separeted by comas: ");
        String[] activitiesString = sc.nextLine().toLowerCase().trim().split(",");

        List<String> activities = new ArrayList<>();
        activities.addAll(activities);

        System.out.println(ApplicationFactory.createSchoolarshipApplication(applicant, grades, activities));
        System.out.println("To exit --> quit");
    }

    private static void conditionalStayApp(Scanner sc, Person applicant) {
        System.out.println("Give me grades separated by comas, (ex. '2,3,4,5,5') : ");
        String[] studentGradesString = sc.nextLine().toLowerCase().trim().split(",");
        List<Double> grades = new ArrayList<>();

        for (String gradesString : studentGradesString) {
            grades.add(Double.parseDouble(gradesString));
        }

        System.out.println("Give me a reason: ");
        String reason = sc.nextLine();

        System.out.println(ApplicationFactory.createConditionalStayApplication(applicant, grades, reason));
        System.out.println("To exit --> quit");
    }
}
