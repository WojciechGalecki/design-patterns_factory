package factory_zadanie3.app;

import factory_zadanie3.main.Person;

import java.time.LocalDateTime;

public class SemesterExtendApplication extends Application {

    private String reason;

    public SemesterExtendApplication(LocalDateTime creationDate, String creationPlace, Person applicantData,
                                     String content, String reason) {
        super(creationDate, creationPlace, applicantData, content);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Thank You for your application: ");
        sb.append("\n Your reason: ")
                .append(reason);

        return sb.toString();
    }
}
