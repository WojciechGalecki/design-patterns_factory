package factory_zadanie3.app;

import factory_zadanie3.main.Person;

import java.time.LocalDateTime;
import java.util.List;

public abstract class ApplicationFactory {


    public static Application createApplication(Person person, String content) {
        return new Application(LocalDateTime.now(), "Gdańsk", person, content);
    }

    public static Application createConditionalStayApplication(Person person, List<Double> grades, String reason) {
        return new ConditionalStayApplication(LocalDateTime.now(), "Gdańsk", person,
                "Please let me stay!", grades, reason);
    }

    public static Application createSchoolarshipApplication(Person person, List<Double> grades,
                                                            List<String> extracurricularActivities) {
        return new SchoolarshipApplication(LocalDateTime.now(), "Gdańsk", person,
                "Gimme more money cuz im smart!", grades, extracurricularActivities);
    }

    public static Application createSemesterExtendApplication(Person person, String reason) {
        return new SemesterExtendApplication(LocalDateTime.now(), "Gdańsk", person,
                "Let me study longer!", reason);
    }

    public static Application createSocialSchoolarshipApplicatio(Person person, List<Double> grades,
                                                                 double totalFamilyIncom) {
        return new SocialSchoolarshipApplication(LocalDateTime.now(), "Gdańsk", person,
                "Gimme more money cuz im poor!", grades, totalFamilyIncom);
    }


}
