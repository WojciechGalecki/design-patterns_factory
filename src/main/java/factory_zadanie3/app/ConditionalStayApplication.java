package factory_zadanie3.app;

import factory_zadanie3.main.Person;
import java.time.LocalDateTime;
import java.util.List;

public class ConditionalStayApplication extends Application {

    private List<Double> grades;
    private String reason;

    public ConditionalStayApplication(LocalDateTime creationDate, String creationPlace, Person applicantData,
                                      String content, List<Double> grades, String reason) {
        super(creationDate, creationPlace, applicantData, content);
        this.grades = grades;
        this.reason = reason;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Thank You for your application: ");
        sb.append("\n Your grades: " )
                .append(grades)
                .append("\n Your reason: ")
                .append(reason);
        return sb.toString();
    }
}
