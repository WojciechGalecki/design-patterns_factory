package factory_zadanie3.app;

import factory_zadanie3.main.Person;

import java.time.LocalDateTime;
import java.util.List;

public class SchoolarshipApplication extends Application {

    private List<Double> grades;
    private List<String> extracurricularActivities;

    public SchoolarshipApplication(LocalDateTime creationDate, String creationPlace, Person applicantData,
                                   String content, List<Double> grades, List<String> extracurricularActivities) {
        super(creationDate, creationPlace, applicantData, content);
        this.grades = grades;
        this.extracurricularActivities = extracurricularActivities;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public List<String> getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(List<String> extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Thank You for your application: ");
        sb.append("\n Your grades: ")
                .append(grades);

        return sb.toString();
    }
}
