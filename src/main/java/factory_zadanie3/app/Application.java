package factory_zadanie3.app;

import factory_zadanie3.main.Person;

import java.time.LocalDateTime;

class Application {
    private LocalDateTime creationDate;
    private String creationPlace;
    private Person applicantData;
    private String content;

    public Application(LocalDateTime creationDate, String creationPlace, Person applicantData, String content) {
        this.creationDate = creationDate;
        this.creationPlace = creationPlace;
        this.applicantData = applicantData;
        this.content = content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationPlace() {
        return creationPlace;
    }

    public void setCreationPlace(String creationPlace) {
        this.creationPlace = creationPlace;
    }

    public Person getApplicantData() {
        return applicantData;
    }

    public void setApplicantData(Person applicantData) {
        this.applicantData = applicantData;
    }

    public String getContents() {
        return content;
    }

    public void setContents(String contents) {
        this.content = contents;
    }
}
