package factory_zadanie3.app;

import factory_zadanie3.main.Person;

import java.time.LocalDateTime;
import java.util.List;

public class SocialSchoolarshipApplication extends Application {

    private List<Double> grades;
    private double totalFamilyIncom;

    public SocialSchoolarshipApplication(LocalDateTime creationDate, String creationPlace, Person applicantData,
                                         String content, List<Double> grades, double totalFamilyIncom) {
        super(creationDate, creationPlace, applicantData, content);
        this.grades = grades;
        this.totalFamilyIncom = totalFamilyIncom;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public double getTotalFamilyIncom() {
        return totalFamilyIncom;
    }

    public void setTotalFamilyIncom(double totalFamilyIncom) {
        this.totalFamilyIncom = totalFamilyIncom;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Thank You for your application: ");
        sb.append("\n Your grades: " )
                .append(grades)
                .append("\n Your total family incom: ")
                .append(totalFamilyIncom);

        return sb.toString();
    }
}
