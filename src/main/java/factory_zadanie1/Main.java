package factory_zadanie1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        AbstractPC comp1 = AsusPC.createAsus1();
        AbstractPC comp2 = AsusPC.createAsus2();
        AbstractPC comp3 = HpPC.createHp1();
        AbstractPC comp4 = HpPC.createHp2();
        AbstractPC comp5 = SamsungPC.createSamsung1();
        AbstractPC comp6 = SamsungPC.createSamsung2();
        AbstractPC comp7 = ApplePC.createApple1();
        AbstractPC comp8 = ApplePC.createApple2();

        List<AbstractPC> list = new ArrayList<>();
        list.add(comp1);
        list.add(comp2);
        list.add(comp3);
        list.add(comp4);
        list.add(comp5);
        list.add(comp6);
        list.add(comp7);
        list.add(comp8);

        for (AbstractPC pc: list) {
            System.out.println(pc);
        }

    }
}
