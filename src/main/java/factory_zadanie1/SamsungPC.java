package factory_zadanie1;

public class SamsungPC extends AbstractPC {

    private SamsungPC(String name, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.SAMSUNG, cpu_power, gpu_power, isOverlocked);
    }

    public static AbstractPC createSamsung1(){
        return new SamsungPC("Sam1",100,0.40,true);
    }

    public static AbstractPC createSamsung2(){
        return new SamsungPC("Sam2",50,0.50,false);
    }
}
