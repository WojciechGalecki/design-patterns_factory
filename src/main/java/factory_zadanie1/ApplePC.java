package factory_zadanie1;

public class ApplePC extends AbstractPC {

    private ApplePC(String name, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.APPLE, cpu_power, gpu_power, isOverlocked);
    }

    public static AbstractPC createApple1(){
        return new ApplePC("Hp1",30,0.39,true);
    }

    public static AbstractPC createApple2(){
        return new ApplePC("Hp2",100,0.60,false);
    }
}
