package factory_zadanie1;

public abstract class AbstractPC {

    private String name;
    private COMPUTER_BRAND computerModel;
    private int cpu_power;
    private double gpu_power;
    private boolean isOverlocked;

    public AbstractPC(String name, COMPUTER_BRAND computerModel, int cpu_power
            , double gpu_power, boolean isOverlocked) {
        this.name = name;
        this.computerModel = computerModel;
        this.cpu_power = cpu_power;
        this.gpu_power = gpu_power;
        this.isOverlocked = isOverlocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public COMPUTER_BRAND getComputerModel() {
        return computerModel;
    }

    public void setComputerModel(COMPUTER_BRAND computerModel) {
        this.computerModel = computerModel;
    }

    public int getCpu_power() {
        return cpu_power;
    }

    public void setCpu_power(int cpu_power) {
        this.cpu_power = cpu_power;
    }

    public double getGpu_power() {
        return gpu_power;
    }

    public void setGpu_power(double gpu_power) {
        this.gpu_power = gpu_power;
    }

    public boolean isOverlocked() {
        return isOverlocked;
    }

    public void setOverlocked(boolean overlocked) {
        isOverlocked = overlocked;
    }

    @Override
    public String toString() {
        return "COMPUTER " + name + " " + computerModel + " " + cpu_power + " " + gpu_power + " " + isOverlocked;
    }
}
