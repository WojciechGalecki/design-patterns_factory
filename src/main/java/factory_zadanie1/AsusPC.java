package factory_zadanie1;

public class AsusPC extends AbstractPC {

    private AsusPC(String name,int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.ASUS, cpu_power, gpu_power, isOverlocked);
    }

    public static AbstractPC createAsus1(){
        return new AsusPC("A1",10,0.19,true);
    }

    public static AbstractPC createAsus2(){
        return new AsusPC("A2",70,0.80,false);
    }
}
