package factory_zadanie1;

public class HpPC extends AbstractPC {

    private HpPC(String name,int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.HP, cpu_power, gpu_power, isOverlocked);
    }

    public static AbstractPC createHp1(){
        return new HpPC("Hp1",30,0.39,true);
    }

    public static AbstractPC createHp2(){
        return new HpPC("Hp2",50,0.90,false);
    }
}
